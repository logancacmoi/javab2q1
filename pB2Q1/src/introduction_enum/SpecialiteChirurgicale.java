package introduction_enum;

public enum  SpecialiteChirurgicale {
    CARDIOLOGIE("Cardiologie") , NEUROLOGIE("Neurologie"), PNEUMOLOGIE("Pneumologie"), STOMATOLOGIE("Stomatologie"), UROLOGIE("Urologie");
    private String libele;

    SpecialiteChirurgicale(String libele){
        this.libele=libele;
    }

    public String getLibele() {
        return libele;
    }
}
