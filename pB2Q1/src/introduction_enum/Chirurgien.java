package introduction_enum;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Chirurgien extends Medecin {

    private SpecialiteChirurgicale[] specialites;

    public Chirurgien(String nom, String prenom, String dateNaissance, SpecialiteChirurgicale[] specialites) {
        super(nom, prenom, dateNaissance);
        setSpecialite(specialites);
        this.specialites = new SpecialiteChirurgicale[]{};
    }


    public void setSpecialite(SpecialiteChirurgicale[] specialiteChirurgicales) {
        this.specialites = Arrays.stream(specialiteChirurgicales).distinct().collect(Collectors.toList()).toArray(new SpecialiteChirurgicale[]{});
    }

    @Override
    public boolean ajouterPatient(Patient patient) {
        if (patient.getEtatSante() == EtatSante.A_OPERER) return super.ajouterPatient(patient);
        return false;
    }

    @Override
    public String getDetails() {
        int[] n = {1};
        String[] spec = {"Cardiologie", "Neurologie", "Pneumologie", "Stomatologie", "Urologie"};
        return super.getDetails() + "\n" + String.join("\n", Arrays.stream(specialites).map(sc -> "Spécialité n°" + (n[0]++) + " :  " + sc.getLibele()).toArray(String[]::new));
    }
}
