package introduction_enum;

public enum  EtatSante {
    A_DIAGNOSTIQUER("A diagnostiquer"), A_OPERER("A opérer"), EN_TRAITEMENT("En traitement"), DECEDE("Décédé"), RETABLI("Rétabli");

    private String libelle;

    EtatSante(String libelle){
        this.libelle = libelle;
    }

    public String getLibelle() {
        return libelle;
    }
}
