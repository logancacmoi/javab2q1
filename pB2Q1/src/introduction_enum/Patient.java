package introduction_enum;

public class Patient extends Personne {
    private EtatSante etatSante;


    public Patient(String nom, String prenom, String dateNaissance, EtatSante etatSante) {
        super(nom, prenom, dateNaissance);
        setEtatSante(etatSante);
    }

    public String getEtatSanteString() {
        return etatSante.getLibelle();
    }


    public EtatSante getEtatSante() {
        return etatSante;
    }

    public void setEtatSante(EtatSante etatSante) {
        this.etatSante=etatSante;
    }

    @Override
    public String getDetails() {
        return super.getDetails()+"\nEtat de santé : "+getEtatSanteString();
    }
}
