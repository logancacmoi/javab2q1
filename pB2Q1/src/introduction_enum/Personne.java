package introduction_enum;

import java.util.concurrent.atomic.AtomicInteger;

public class Personne {
    private static AtomicInteger atomicInteger= new AtomicInteger();
    public final Integer ID;
    private String nom;
    private String prenom;
    private String dateNaissance;



    public Personne(String nom, String prenom, String dateNaissance){
        ID=atomicInteger.getAndIncrement();
        setPrenom(prenom);
        setNom(nom);
        setDateNaissance(dateNaissance);
    }



    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return nom.equals(personne.nom) && prenom.equals(personne.prenom);
    }


    public String toString() {
        return nom+" "+prenom;
    }


    public String getDetails() {
        return "Identifiant : "+ID+"\n"+
        "Nom : "+nom+"\n"+
        "Prénom : "+prenom+"\n"+
        "Date de naissance : "+dateNaissance;
    }

}
