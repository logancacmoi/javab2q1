package introduction;

import java.util.Arrays;

public class Chirurgien extends Medecin implements SpecialiteChirurgicale {

    private int[] specialites;

    public Chirurgien(String nom, String prenom, String dateNaissance, int[] specialites) {
        super(nom, prenom, dateNaissance);
        setSpecialite(specialites);
    }

    @Override
    public boolean setSpecialite(int[] specialites) {
        if (Arrays.stream(specialites).anyMatch(value -> value > 5 || value < 1)) return false;
        this.specialites = Arrays.stream(specialites).distinct().toArray();
        return true;
    }

    @Override
    public boolean ajouterPatient(Patient patient) {
        if (patient.getEtatSante() == EtatSante.A_OPERER) return super.ajouterPatient(patient);
        return false;
    }

    @Override
    public String getDetails() {
        int[] n = {1};
        String[] spec = {"Cardiologie", "Neurologie", "Pneumologie", "Stomatologie", "Urologie"};
        return super.getDetails() + "\n" + String.join("\n", Arrays.stream(specialites).mapToObj(value -> "Spécialité n°" + (n[0]++) + " : " + spec[value - 1]).toArray(String[]::new));
    }
}
