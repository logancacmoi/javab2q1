package introduction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Hopital {
    private List<Personne> personnes;

    public Hopital() {
        personnes = new ArrayList<>();
    }

    public boolean ajouter(Personne personne) {
        if (personnes.contains(personne)) return false;
        return personnes.add(personne);
    }

    public boolean supprimerPersonne(Personne personne) {
        return personnes.remove(personne);
    }


    public String getListeMedecins() {
        return Arrays.toString(personnes.stream().filter(personne -> personne instanceof Medecin).map(Personne::toString).toArray(String[]::new));
    }

    public String getListePatients() {
        return Arrays.toString(personnes.stream().filter(personne -> personne instanceof Patient).map(Personne::toString).toArray(String[]::new));
    }

    public void trierPersonnes() {
        personnes.sort((o1, o2) -> o1.getNom().compareTo(o2.getNom()) != 0 ? o1.getNom().compareTo(o2.getNom()) : o1.getPrenom().compareTo(o2.getPrenom()));
    }

    @Override
    public String toString() {
        return Arrays.toString(personnes.toArray(new Personne[]{}));
    }


}
