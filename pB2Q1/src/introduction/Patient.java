package introduction;

public class Patient extends Personne implements EtatSante {
    private int etatSante;


    public Patient(String nom, String prenom, String dateNaissance, int etatSante) {
        super(nom, prenom, dateNaissance);
        setEtatSante(etatSante);
    }

    public String getEtatSanteString() {
        String ret = "indéfini";
        switch (etatSante) {
            case EtatSante.A_DIAGNOSTIQUER:
                ret = "A diagnostiquer";
                break;
            case EtatSante.A_OPERER:
                ret = "A opérer";
                break;
            case EtatSante.EN_TRAITEMENT:
                ret = "En traitement";
                break;
            case EtatSante.DECEDE:
                ret = "Décédé";
                break;
            case EtatSante.RETABLI:
                ret = "Rétabli";
                break;
            default:
                ret = "inéfini";
        }
        return ret;
    }


    public int getEtatSante() {
        return etatSante;
    }

    public boolean setEtatSante(int etatSante) {
        if (etatSante > EtatSante.RETABLI || etatSante < EtatSante.A_DIAGNOSTIQUER) return false;
        this.etatSante = etatSante;
        return true;
    }

    @Override
    public String getDetails() {
        return super.getDetails() + "\nEtat de santé : " + getEtatSanteString();
    }
}
