package introduction;

import java.util.ArrayList;
import java.util.List;

public class Medecin extends Personne {


    private List<Patient> patients;
    private int[] specialites;

    public Medecin(String nom, String prenom, String dateNaissance) {
        super(nom, prenom, dateNaissance);
        patients = new ArrayList<>();
    }


    public boolean ajouterPatient(Patient patient) {
        if (patients.contains(patient)) return false;
        return patients.add(patient);
    }

    public boolean supprimerPatient(Patient patient) {
        return patients.remove(patient);
    }

    public int rechercherPatient(Patient patient) {
        return patients.indexOf(patient);
    }

    public boolean remplacerPatient(Patient patient1, Patient patient2) {
        if (patients.contains(patient1) && !patients.contains(patient2)) {
            patients.set(patients.indexOf(patient1), patient2);
            return true;
        }
        return false;
    }

    public int nombrePatients() {
        return patients.size();
    }

    public String getDetails() {
        return "Identifiant : " + ID + "\n" +
                "Nom : " + getNom() + "\n" +
                "Prénom : " + getPrenom() + "\n" +
                "Date de naissance : " + getDateNaissance() + "\n" +
                String.join("\n", patients.stream().map(patient -> "Patient n°" + (patients.indexOf(patient) + 1) + " :\n" + patient.getNom() + " " + patient.getPrenom()).toArray(String[]::new));
        
    }


}
