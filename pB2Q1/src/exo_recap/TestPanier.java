package exo_recap;

import exo_recap.exceptions.PrixDepasseException;
import exo_recap.exceptions.QuantiteException;
import exo_recap.exceptions.StockInsuffisantException;
import exo_recap.modele.Panier;
import exo_recap.modele.Produit;
import utils.Serialisation;

public class TestPanier {
    public static void main(String[] args) {
        Panier panier1 = new Panier();
        Panier panier2 = new Panier();
        Panier panier3 = new Panier();

        Produit produit1= new Produit("truc1", 10., 15);
        Produit produit2= new Produit("truc2", 13., 15);
        Produit produit3= new Produit("truc3", 13., 15);
        Produit produit4= new Produit("truc4", 13., 15);

        try {
            panier1.ajouterProduit(produit1, 9);
            panier1.ajouterProduit(produit2, 1);
            panier3.ajouterProduit(produit3, 4);
            panier1.supprimer(produit1, 3);
        } catch (QuantiteException | StockInsuffisantException | PrixDepasseException e) {
            e.printStackTrace();
        }

        Serialisation.writeObject("test.bin", panier1);
        Panier panier= (Panier) Serialisation.readObject("test.bin");
        System.out.println();
    }
}
