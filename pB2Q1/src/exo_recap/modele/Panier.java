package exo_recap.modele;

import exo_recap.exceptions.PrixDepasseException;
import exo_recap.exceptions.QuantiteException;
import exo_recap.exceptions.StockInsuffisantException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Panier implements Serializable {
    private List<Commande> commandes;
    public double TOTAL_MAX = 400.;


    public Panier() {
        commandes = new ArrayList<>();
    }

    public void ajouterProduit(Produit produit, int qt_commande) throws QuantiteException, StockInsuffisantException, PrixDepasseException {
        if (produit != null && qt_commande>0) {
            if (calculerTotal() + produit.getPrix_unitaire() * qt_commande > TOTAL_MAX)
                throw new PrixDepasseException(TOTAL_MAX);
            Commande commande = getCommande(produit);
            if (commande == null) commandes.add(new Commande(produit, qt_commande));
            else {
                if (commande.getQt_commande() + qt_commande > 10) throw new QuantiteException();
                commande.setQt_commande(commande.getQt_commande() + qt_commande);
            }
        }
    }

    public void supprimer(Produit produit, int qt_a_supprimer) {
        if (produit != null && qt_a_supprimer > 0) {
            Commande commande = getCommande(produit);
            if (commande != null) {
                Math.min(qt_a_supprimer, commande.getQt_commande());
                try {
                    commande.setQt_commande(commande.getQt_commande()-qt_a_supprimer);
                } catch (StockInsuffisantException e) {
                    e.printStackTrace();
                }
                if (commande.getQt_commande() <= 0) commandes.remove(commande);
            }
        }
    }

    public double calculerTotal() {
        return commandes.stream().mapToDouble(value -> value.getProduit().getPrix_unitaire() * value.getQt_commande()).sum();
    }

    private Commande getCommande(Produit produit) {
        return commandes.stream().filter(commande -> commande.getProduit().equals(produit)).findFirst().orElse(null);
    }


}
