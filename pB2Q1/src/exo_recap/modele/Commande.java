package exo_recap.modele;

import exo_recap.exceptions.StockInsuffisantException;

import java.io.Serializable;

public class Commande implements Serializable {
    private Produit produit;
    private int qt_commande;



    public Commande(Produit produit, int qt_commande) throws StockInsuffisantException {
        this.produit = produit;
        this.qt_commande = Math.max(qt_commande, 0);
        if (produit==null) throw new IllegalArgumentException("Le produit ne peut pas être null");
        if (qt_commande>produit.getQt_stock()) throw new StockInsuffisantException(produit, qt_commande);

        produit.setQt_stock(produit.getQt_stock()-qt_commande);
    }

    public Produit getProduit() {
        return produit;
    }

    public int getQt_commande() {
        return qt_commande;
    }

    public void setQt_commande(int qt_commande) throws StockInsuffisantException {
        qt_commande=Math.max(qt_commande, 0);
        if (produit.getQt_stock()-qt_commande<0) throw new StockInsuffisantException(produit, qt_commande);
        produit.setQt_stock(produit.getQt_stock()+this.qt_commande-qt_commande);
        this.qt_commande=qt_commande;
    }

    @Override
    public String toString() {
        return "Produit: " + produit + "";
    }
}
