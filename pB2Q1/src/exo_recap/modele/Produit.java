package exo_recap.modele;

import java.io.Serializable;

public class Produit implements Serializable {
    private String libelle;
    private double prix_unitaire;
    private int qt_stock;

    public Produit(String libelle, double prix_unitaire, int qt_stock) {
        setLibelle(libelle);
        setPrix_unitaire(prix_unitaire);
        setQt_stock(qt_stock);
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        if (libelle == null || (libelle = libelle.trim()).isEmpty()) this.libelle = "vide";
        else this.libelle = libelle;
    }

    public double getPrix_unitaire() {
        return prix_unitaire;
    }

    public void setPrix_unitaire(double prix_unitaire) {
        this.prix_unitaire = Math.max(prix_unitaire, 0.);
    }

    public int getQt_stock() {
        return qt_stock;
    }

    public void setQt_stock(int qt_stock) {
        this.qt_stock = Math.max(qt_stock, 0);
    }

    public double getValStock() {
        return prix_unitaire*qt_stock;
    }

    @Override
    public String toString() {
        return libelle+", "+qt_stock+", "+prix_unitaire;
    }
}
