package exo_recap.exceptions;

public class PrixDepasseException extends Exception {
    public PrixDepasseException(double prixMax){
        super("Le montant total des articles du panier ne peut dépasser le plafond de : "+prixMax);
    }
}
