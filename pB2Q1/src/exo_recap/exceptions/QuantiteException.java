package exo_recap.exceptions;

public class QuantiteException extends Exception {
    public QuantiteException(){
        super("Interdiction passer une commande de plus de 10 produits identiques");
    }
}
