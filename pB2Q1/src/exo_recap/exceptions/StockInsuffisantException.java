package exo_recap.exceptions;

import exo_recap.modele.Produit;

public class StockInsuffisantException extends Exception {
    public StockInsuffisantException(Produit produit, int qt_commande){
        super("La quantité commandé : "+qt_commande+" est plus grande que la quantité disponible : "+produit.getQt_stock());
    }
}
