package utils;

import java.io.*;

public class Serialisation {

    public static void writeObject(String file, Object o){
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))){
            out.writeObject(o);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeObject(File file, Object o) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            out.writeObject(o);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object readObject(String file){
        try(ObjectInputStream in= new ObjectInputStream(new FileInputStream(file))){
            return in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object readObject(File file){
        try(ObjectInputStream in= new ObjectInputStream(new FileInputStream(file))){
            return in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }



}
