package map.modele;


import java.io.Serializable;
import java.util.Objects;

public class Produit implements Serializable {
    public final String REFERENCE;
    private String nom;
    private String description;
    private double prix;

    public Produit(String reference, String nom, String description, double prix) {
        REFERENCE = reference;
        setNom(nom);
        setDescription(description);
        setPrix(prix);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produit produit = (Produit) o;
        return Objects.equals(REFERENCE, produit.REFERENCE);
    }

    @Override
    public int hashCode() {
        return Objects.hash(REFERENCE);
    }

}
