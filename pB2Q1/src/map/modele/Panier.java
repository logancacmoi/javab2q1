package map.modele;
import map.exeptions.ArticleNonPresentException;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Panier implements Serializable {
    private Map<Produit, Integer> produits;

    public Panier(){
        produits= new HashMap<>();
    }


    public void ajouter(Produit produit, int n) {
        if (n>0){
            if (produits.computeIfPresent(produit, (k, v)->v+n)==null) produits.put(produit,n);
        }
    }


    public void ajouter(Produit produit) {
        if (produits.computeIfPresent(produit, (k, v) -> v+1) == null) produits.put(produit, 1);
    }

    public void supprimer(Produit produit) throws ArticleNonPresentException {
        if (produits.remove(produit)==null) throw new ArticleNonPresentException(produit);
    }

    public void supprimer(Produit produit, int n) throws ArticleNonPresentException {
        Integer value= produits.get(produit);
        if (value==null) throw new ArticleNonPresentException(produit);
        else if (n>0) {
            if (value.compareTo(n) <= 0)
                produits.remove(produit);
            else produits.computeIfPresent(produit, (k, v) -> v - n);
        }
    }

    public double calculerTotal(){
        double[] total={0.};
        produits.forEach((produit, integer) -> total[0] +=produit.getPrix()*integer);
        return total[0];
    }

    public void vider(){
        produits.clear();
    }

    public Produit getProduit(String reference){
        Produit[] p= new Produit[]{null};
        produits.entrySet().stream().filter(x->x.getKey().REFERENCE.equals(reference)).findFirst().ifPresent(x->p[0]=x.getKey());
        return p[0];
    }

    @Override
    public String toString() {
        StringBuilder ret= new StringBuilder("___________________________________________________________________________________________________________________________________________\n");
        ret.append(String.format("| %-30s | %-30s | %-45s | %8s | %-10s |", "Référence", "Produit", "Description", "Quantité", "Prix")).append("\n");
        ret.append("|________________________________|________________________________|_______________________________________________|__________|____________|\n");
        String[] ret1=produits.entrySet().stream().map(x -> String.format("| %-30s | %-30s | %-45s | %8s | %-10s |", x.getKey().REFERENCE, x.getKey().getNom(), x.getKey().getDescription(),x.getValue(), x.getKey().getPrix()*x.getValue())).toArray(String[]::new);
        ret.append(String.join("\n", ret1));
        ret.append("\n|________________________________|________________________________|_______________________________________________|__________|____________|\n");
        ret.append(String.format("%124s | %-10.2f |", "|  total: ", this.calculerTotal())).append("\n");
        ret.append(String.format("%140s","|__________|____________|\n"));
        return ret.toString();
    }
}
