package map;

import map.exeptions.ArticleNonPresentException;
import map.modele.Panier;
import map.modele.Produit;
import utils.Serialisation;

public class Main {
    public static void main(String[] args) {
        Panier panier= new Panier();
        Produit produit= new Produit("TEST","carte", "c'est la carte", 69.00 );
        Produit produit1=new Produit("TEST2", "Babouche","singe qui porte des bottes", 250.);
        panier.ajouter(produit, 5);
        panier.ajouter(produit1, 1);
        try {
            panier.supprimer(produit, 3);
            panier.supprimer(new Produit("TEST1","fourchette","couteau",4.));
        } catch (ArticleNonPresentException e) {
            e.printStackTrace();
        }

        Produit p=panier.getProduit("TEST");
        Serialisation.writeObject("testPanier.bin", panier);
        Panier panier1= (Panier) Serialisation.readObject("testPanier.bin");
        System.out.println(panier1);

    }

}
