package map.tests;

import map.exeptions.ArticleNonPresentException;
import map.modele.Panier;
import map.modele.Produit;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class PanierTest {

    private Panier panier;
    private Produit produit1;
    private Produit produit2;
    private Map<Produit, Integer> produits;


    /*
    gt   = greater than
    lt   = less than
    gtoe = greater than or equals
    ltoe = less than or equals
    eq   = equals
     */


    @org.junit.Before
    public void setUp() throws Exception {
        panier= new Panier();
        produit1= new Produit("TEST","bol","bol",5.);
        produit2= new Produit("TEST1","assiette","pas un bol",6.);
        Field fieldMap= Panier.class.getDeclaredField("produits");
        fieldMap.setAccessible(true);
        produits= (Map<Produit, Integer>) fieldMap.get(panier);
        fieldMap.setAccessible(false);
    }

    @Test
    public void constucteur() throws NoSuchFieldException, IllegalAccessException {
        Panier panier= new Panier();
        Field fieldMap= Panier.class.getDeclaredField("produits");
        fieldMap.setAccessible(true);
        produits= (Map<Produit, Integer>) fieldMap.get(panier);
        fieldMap.setAccessible(false);


        assertTrue(produits instanceof HashMap);
    }

    @Test
    public void ajouter(){
        panier.ajouter(produit1);
        assertTrue(produits.containsKey(produit1));
        assertEquals(1, (int) produits.get(produit1));
    }

    @Test
    public void ajouter_present(){
        panier.ajouter(produit1);
        panier.ajouter(produit1);
        panier.ajouter(produit1);
        assertEquals(3, (int) produits.get(produit1));
    }

    @Test
    public void ajouter_n_produits(){
        panier.ajouter(produit1, 4);
        assertEquals(4, (int) produits.get(produit1));
    }

    @Test
    public void ajouter_n_produits_present(){
        panier.ajouter(produit1, 4);
        panier.ajouter(produit1, 4);
        assertEquals(4+4, (int) produits.get(produit1));
    }

    @Test
    public void ajouter_n_ltoe_0(){
        panier.ajouter(produit1,0);
        assertFalse(produits.containsKey(produit1));
    }


    @Test
    public void supprimer() throws ArticleNonPresentException {
        panier.ajouter(produit1);
        panier.supprimer(produit1);
        assertFalse(produits.containsKey(produit1));
    }



    @Test
    public void supprimer_n_produits() throws ArticleNonPresentException {
        panier.ajouter(produit1,4);
        panier.supprimer(produit1,2);
        assertEquals((int) produits.get(produit1), 4-2);
    }

    @Test
    public void supprimer_n_gtoe_produits() throws ArticleNonPresentException {
        panier.ajouter(produit1, 5);
        panier.supprimer(produit1, 5);
        assertFalse(produits.containsKey(produit1));
    }

    @Test
    public void supprimer_n_ltoe_0_produits() throws ArticleNonPresentException {
        panier.ajouter(produit1, 4);
        panier.supprimer(produit1, 0);
        assertEquals((int) produits.get(produit1), 4);
    }

    @Test(expected = ArticleNonPresentException.class)
    public void supprimer_n_produit_absent() throws ArticleNonPresentException {
        panier.supprimer(produit1, 0);
    }

    @Test(expected = ArticleNonPresentException.class)
    public void supprimer_absent() throws ArticleNonPresentException {
        panier.supprimer(produit1);
    }

    @Test
    public void calculerTotal(){
        double total=0.;
        panier.ajouter(produit1);
        total+=produit1.getPrix()*produits.get(produit1);
        panier.ajouter(produit2);
        total+=produit2.getPrix()*produits.get(produit2);


        assertEquals(total, panier.calculerTotal(), 0);
    }

    @Test
    public void vider(){
        panier.ajouter(produit1);
        panier.vider();
        assertEquals(produits.size(),0);
    }

    @Test
    public void getProduit(){
        panier.ajouter(produit1);
        assertSame(produit1, panier.getProduit(produit1.REFERENCE));
    }

    public void getProduit_absent(){
        assertNull(panier.getProduit(produit1.REFERENCE));
    }






}