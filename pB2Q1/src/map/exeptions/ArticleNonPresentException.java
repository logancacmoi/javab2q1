package map.exeptions;

import map.modele.Produit;

public class ArticleNonPresentException extends Exception {
    public ArticleNonPresentException(Produit produit){
        super("Le produit ref:"+produit.REFERENCE+"("+produit.getNom()+") n'est pas présent dans le panier");
    }
}
