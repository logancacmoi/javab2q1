package arrayList.polygone;

public class Main {
    public static void main(String[] args) {

        Polygone carre_a_8_cotes = new Polygone();
        Point booba = new Point(0, 1);
        Point kaaris = new Point(1, 0);
        Point lepen = new Point(2, 0);
        Point trump = new Point(3, 1);
        Point sarkozy = new Point(3, 2);
        Point hollande = new Point(2, 3);
        Point carlos = new Point(1, 3);
        Point ben_laden = new Point(0, 2);

        carre_a_8_cotes.ajouter(booba, kaaris, lepen, trump, sarkozy, hollande, carlos, ben_laden);
        System.out.println(carre_a_8_cotes);

        carre_a_8_cotes.enlever(0);
        System.out.println(carre_a_8_cotes);


        carre_a_8_cotes.ajouter(ben_laden);
        /*
        30 > taille de la liste(8)
         */

        carre_a_8_cotes.changer(30, 40, 45);


        carre_a_8_cotes.ajouter(ben_laden);
        carre_a_8_cotes.changer(0, 40, 45);
        carre_a_8_cotes.ajouter(booba);
        System.out.println(carre_a_8_cotes);
        System.out.println(carre_a_8_cotes.diagonales());


    }
}
