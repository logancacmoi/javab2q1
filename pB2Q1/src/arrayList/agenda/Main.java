package arrayList.agenda;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args)  {

        Scanner scanner = new Scanner(System.in);
        Agenda agenda = new Agenda();
        agenda.ajouter(new Personne("Ben Laden", "Oussama", "Abbottābad", "n'est pas diponible pour le moment"));
        agenda.ajouter(new Personne("Durif", "Sylvain", "Bugarach", "101"));
        agenda.ajouter(new Personne("Berwick", "Ricky", "Canada", "5"));
        agenda.ajouter(new Personne("Cadillac", "Johnny", "https://johnnycadillac.skyrock.com/", "9"));

        agenda.trier();

        do {
            System.out.println("Agenda: ");
            System.out.println("Choisissez une action: ");
            System.out.println("\t1: Ajouter une personne");
            System.out.println("\t2: Affichage complet");
            System.out.println("\t3: Recherche d'une personne");
            System.out.println("\t4: Modifier une personne");
            System.out.println("\t5: quitter l'application");
            System.out.print("Votre choix: ");
            String choice = scanner.next();

            switch (choice) {
                case "1":
                    String nom;
                    String prenom;
                    String adresse;
                    String num_tel;
                    System.out.print("Entrez son nom: ");
                    scanner.nextLine();
                    nom = scanner.nextLine();
                    System.out.print("Entrez son prénom: ");
                    prenom = scanner.nextLine();
                    System.out.print("Entrez son adresse: ");
                    adresse = scanner.nextLine();
                    System.out.print("Entrez son numéro de téléphone: ");
                    num_tel = scanner.nextLine();
                    if (agenda.ajouter(new Personne(nom, prenom, adresse, num_tel)))
                        System.out.println(nom + " a été ajouté(e) à l'agenda");
                    else System.out.println("cette personne est déja présente dans l'agenda");
                    break;
                case "2":
                    System.out.println(agenda);
                    break;
                case "3":
                    System.out.println("Entrez le nom de la personne que vous rechercher: ");
                    scanner.nextLine();
                    nom = scanner.nextLine();
                    System.out.println(nom + "test");
                    List<Personne> personnes = agenda.chercher(nom);

                    System.out.println();
                    if (personnes.size() == 0) System.out.println("Personne n'a été trouvé");
                    else System.out.println(
                            String.format("\n\t%-30s %-30s %-45s  %-15s", "NOM: ", "PRÉNOM: ", "ADRESSE: ", "NUMÉRO DE TÉLÉPHONE: \n") +
                                    String.join("", personnes.stream().map(personne -> "\n-\t" + personne.toString()).toArray(String[]::new))
                    );
                    break;
                case "4":
                    System.out.print("Entrez le nom de la personne que vous souhaitez modifier: ");
                    scanner.nextLine();
                    nom = scanner.nextLine();
                    Personne oldPersonne = new Personne(nom, "", "", "");
                    if (!agenda.contains(oldPersonne)) System.out.println("Personne ne correspond à ce nom");
                    else {
                        boolean loop = false;
                        do {
                            System.out.print("nouveau nom : ");

                            nom = scanner.nextLine();
                            Personne newPersonne = new Personne(nom, "", "", "");
                            if (agenda.contains(newPersonne) && !newPersonne.getNom().equals(oldPersonne.getNom())) {
                                System.out.println("Une personne portant ce nom est déja présente dans l'agenda\n" +
                                        "1: choisir un autre nom\nautre: annuler la modification");
                                choice = scanner.nextLine();
                                if (!choice.equals("1")) {
                                    nom = null;
                                } else loop = true;
                            } else {
                                loop = false;
                            }

                        } while (loop);

                        if (nom != null) {
                            System.out.print("nouveau prénom : ");
                            prenom = scanner.nextLine();

                            System.out.print("nouvelle adresse : ");
                            adresse = scanner.nextLine();
                            System.out.print("nouveau numéro de téléphone : ");
                            num_tel = scanner.nextLine();
                            if (agenda.modifier(oldPersonne, new Personne(nom, prenom, adresse, num_tel)))
                                System.out.println("La personne a bien été modifiée");
                            else System.out.println("Modification annulée");


                        }


                    }

                    break;
                case "5":
                    System.out.println("Aurevoir");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Ce choix n'est pas proposé");
            }

            System.out.println("\n\n\n");
        } while (true);
    }
}
