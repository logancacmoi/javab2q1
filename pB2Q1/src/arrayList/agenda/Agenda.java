package arrayList.agenda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Agenda {

    List<Personne> personnes;

    public Agenda() {
        personnes = new ArrayList<>();
    }

    public boolean contains(Personne personne) {
        return personnes.contains(personne);
    }

    public boolean ajouter(Personne personne) {
        if (personnes.contains(personne)) return false;
        else return personnes.add(personne);
    }


    public boolean supprimer(Personne personne) {
        return personnes.remove(personne);
    }

    public boolean modifier(Personne oldPersonne, Personne newPersonne) {
        if (personnes.contains(newPersonne) && !newPersonne.equals(oldPersonne) ||
           !personnes.contains(oldPersonne)
        ) return false;

        personnes.add(newPersonne);
        personnes.remove(oldPersonne);
        return true;
    }

    public void trier() {
        Collections.sort(personnes);
    }

    @Override
    public String toString() {
        return "Agenda:" + String.format("\n\t%-30s %-30s %-45s  %-15s", "NOM: ", "PRÉNOM: ", "ADRESSE: ", "NUMÉRO DE TÉLÉPHONE: ") + String.join("", personnes.stream().map(personne -> "\n\t" + personne.toString()).toArray(String[]::new));
    }

    public List<Personne> chercher(String nom) {
        if (nom != null) {
            return personnes.stream().filter(personne -> personne.getNom().toLowerCase().contains(nom.toLowerCase())).collect(Collectors.toList());
        } else return Collections.emptyList();
    }

    public List<Personne> chercher(String nom, boolean nom_complet) {
        if (nom == null) return Collections.emptyList();
        if (nom_complet) {
            return personnes.stream().filter(personne -> personne.getNom().toLowerCase().equals(nom.toLowerCase())).collect(Collectors.toList());
        } else return chercher(nom);
    }
}
