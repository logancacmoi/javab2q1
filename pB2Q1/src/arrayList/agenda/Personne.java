package arrayList.agenda;
public class Personne implements Comparable<Personne>{


    private String nom;
    private String prenom;
    private String adresse;
    private String num_tel;


    public Personne(String nom, String prenom, String adresse, String num_tel){
        setNom(nom);
        setPrenom(prenom);
        setAdresse(adresse);
        setNum_tel(num_tel);
    }

    public void setNum_tel(String num_tel) {
        this.num_tel = num_tel;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNum_tel() {
        return num_tel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return nom.equals(personne.nom);
    }


    @Override
    public int compareTo(Personne o) {
        return this.nom.compareTo(o.getNom());
    }

    @Override
    public String toString() {
        return String.format("%-30s %-30s %-45s  %-15s", nom, prenom, adresse, num_tel);
    }
}
