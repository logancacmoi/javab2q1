package arrayList.nbAleatoires;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Integer> listRandom1 = new ArrayList<>();
        List<Integer> listRandom2 = new ArrayList<>();
        int a, i;
        Random random = new Random();


        /*
        Ajout de 30 nombres aléatoires différents dans la 1ère liste
        */
        i = 0;
        while (i < 30) {
            /*
            Affectation de a et vérification de sa présence dans la liste
            -pas présent -> a est ajouté et i est incrémenté
            */
            if (!listRandom1.contains(a = random.nextInt(100) + 1)) {
                listRandom1.add(a);
                i++;
            }

        }

        /*
        Ajout de 30 nombres aléatoires différents dans la 2ème liste
        */

        while (listRandom2.size() < 30) {
            /*
            Affectation de a et vérification de sa présence dans les 2 listes
            -pas présent -> a est ajouté et i est incrémenté
            */
            if (!listRandom1.contains(a = random.nextInt(100) + 1) && !listRandom2.contains(a)) listRandom2.add(a);
        }

        /*
        Tri des 2 listes
        */

        Collections.sort(listRandom1);
        Collections.sort(listRandom2);

        /* ou
        listRandom1.sort(Integer::compareTo);
        listRandom2.sort(Integer::compareTo);
        */


        /*
        Affichage des liste grace à la méthode toString()
        remarque: System.out.println() fait appel à toString
         */

        /*
        lambda
         */
        System.out.println("Première liste: ");
        listRandom1.forEach(integer -> System.out.print(integer + ", "));

        System.out.println("\n\nDeuxième liste: ");
        listRandom2.forEach(integer -> System.out.print(integer + ", "));

        /*
        Sans lambda
         */
        System.out.println("\n\nPremière liste: ");
        for (Integer integer : listRandom1) {
            System.out.print(integer + ", ");
        }

        System.out.println("\n\nDeuxième liste: ");
        for (Integer integer : listRandom2) {
            System.out.print(integer + ", ");
        }

        /*
        Stream + lambda
         */
        System.out.println(
                "\n\nPremière liste: \n" +
                        String.join(", ", listRandom1.stream().map(Object::toString).toArray(String[]::new)) +
                        "\n\nDeuxième liste: \n" +
                        String.join(", ", listRandom2.stream().map(Object::toString).toArray(String[]::new))
        );

        /*
        Arrays.toString()
         */
        System.out.println(
                "\nPremière liste: \n" +
                        Arrays.toString(listRandom1.toArray()) +
                        "\n\nDeuxième liste: \n" +
                        Arrays.toString(listRandom2.toArray())
        );


    }
}
