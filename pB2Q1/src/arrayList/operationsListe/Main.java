package arrayList.operationsListe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> liste1 = new ArrayList<>(Arrays.asList("George", "Jim", "John", "Blake", "Kevin", "Michael"));
        List<String> liste2 = new ArrayList<>(Arrays.asList("George", "Katie", "Kevin", "Michelle", "Ryan"));

        List<String> listeUnion = new ArrayList<>(liste1);
        listeUnion.removeAll(liste2);
        listeUnion.addAll(liste2);

        List<String> listeDifferences = new ArrayList<>(liste1);
        listeDifferences.addAll(liste2);
        listeDifferences.removeIf(s -> liste1.contains(s) && liste2.contains(s));

        List<String> listeIntersection = new ArrayList<>(liste1);
        listeIntersection.retainAll(liste2);


        /*
        tri des listes
         */
        liste1.sort(String::compareTo);
        liste2.sort(String::compareTo);
        listeDifferences.sort(String::compareTo);
        listeIntersection.sort(String::compareTo);
        listeUnion.sort(String::compareTo);

        System.out.println("Liste 1:\n" + Arrays.toString(liste1.toArray()));
        System.out.println("Liste 2:\n" + Arrays.toString(liste2.toArray()));
        System.out.println("\nListe intersection:\n" + Arrays.toString(listeIntersection.toArray()));
        System.out.println("\nListe union:\n" + Arrays.toString(listeUnion.toArray()));
        System.out.println("\nListe différences:\n" + Arrays.toString(listeDifferences.toArray()));
    }
}
