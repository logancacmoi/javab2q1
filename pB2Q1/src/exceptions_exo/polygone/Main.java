package exceptions_exo.polygone;

import exceptions_exo.polygone.exceptions.DoublonException;

public class Main {
    public static void main(String[] args) {

        Polygone carre_a_8_cotes = new Polygone();
        Point booba = new Point(0, 1);
        Point kaaris = new Point(1, 0);
        Point lepen = new Point(2, 0);
        Point trump = new Point(3, 1);
        Point sarkozy = new Point(3, 2);
        Point hollande = new Point(2, 3);
        Point carlos = new Point(1, 3);
        Point ben_laden = new Point(0, 2);

        carre_a_8_cotes.ajouter(booba, kaaris, lepen, trump, sarkozy, hollande, carlos, ben_laden);
        System.out.println(carre_a_8_cotes);

        carre_a_8_cotes.enlever(0);
        System.out.println(carre_a_8_cotes);


        /*
        30 > taille de la liste(8)
         */
        try {
            carre_a_8_cotes.ajouter(ben_laden);
        } catch (DoublonException e) {
            e.printStackTrace();
        }
        carre_a_8_cotes.changer(30, 40, 45);


        try {
            carre_a_8_cotes.ajouter(booba);
            carre_a_8_cotes.ajouter(ben_laden);
        } catch (DoublonException e) {
            e.printStackTrace();
        }
        carre_a_8_cotes.changer(-1, 40, 45);
        System.out.println(carre_a_8_cotes);
        System.out.println("diagonales: "+carre_a_8_cotes.diagonales());


    }
}
