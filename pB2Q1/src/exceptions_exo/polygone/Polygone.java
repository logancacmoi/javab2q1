package exceptions_exo.polygone;

import exceptions_exo.polygone.exceptions.DoublonException;

import java.util.ArrayList;
import java.util.List;

public class Polygone {

    private List<Point> points;

    public Polygone() {
        points = new ArrayList<>();
    }

    public void ajouter(Point p) throws DoublonException {
        if (points.contains(p)) throw new DoublonException(p);
        if (!points.contains(p)) points.add(p);
    }

    public void ajouter(Point... points) {
        for (Point point : points) {
            try {
                ajouter(point);
            } catch (DoublonException e) {
                e.printStackTrace();
            }
        }
    }

    public void enlever(Point p) {
        points.remove(p);
    }

    public void enlever(int ind) {
        points.remove(ind);
    }

    public void changer(int ind, int x, int y) {
        Point newPoint = new Point(x, y);

        try {
            if (points.contains(newPoint)) points.remove(points.get(ind));
            else points.set(ind, newPoint);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }


    }

    public int diagonales() {
        int n = points.size();
        return n * (n - 3) / 2;
    }

    @Override
    public String toString() {

        return "Points:\n" + String.join("\n", points.stream().map(Point::toString).toArray(String[]::new));
    }
}
