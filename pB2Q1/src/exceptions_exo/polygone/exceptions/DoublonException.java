package exceptions_exo.polygone.exceptions;

import exceptions_exo.polygone.Point;

public class DoublonException extends Exception {
    public DoublonException(Point point){
        super("Ce point : x: "+point.getX()+", y: "+point.getY()+" est déja présent dans ce polygone");
    }
}
