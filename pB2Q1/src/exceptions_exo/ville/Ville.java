package exceptions_exo.ville;

import exceptions_exo.ville.exceptions.NbHabitant_ppq0_Exception;

public class Ville {
    private String nom;
    private long nbHabitant;

    public Ville(String nom){
        try {
            setNbHabitant(1000);
        } catch (NbHabitant_ppq0_Exception e) {
            e.printStackTrace();
        }
        setNom(nom);
    }

    public Ville(String nom, long nbHabitant){
        try {
            setNbHabitant(1000);
            setNbHabitant(nbHabitant);
        } catch (NbHabitant_ppq0_Exception e) {
            e.printStackTrace();
        }
        setNom(nom);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getNbHabitant() {
        return nbHabitant;
    }

    public void setNbHabitant(long nbHabitant) throws NbHabitant_ppq0_Exception {
        if (nbHabitant>0) this.nbHabitant=nbHabitant;
        else throw new NbHabitant_ppq0_Exception(nbHabitant);
    }

    @Override
    public String toString() {
        return "Ville{" +
                "nom='" + nom + '\'' +
                ", nbHabitant=" + nbHabitant +
                '}';
    }
}
