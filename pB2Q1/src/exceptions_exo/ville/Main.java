package exceptions_exo.ville;

import exceptions_exo.ville.exceptions.NbHabitant_ppq0_Exception;

public class Main {
    public static void main(String[] args) {
        Ville ville= new Ville("test");
        System.out.println(ville);
        try {
            ville.setNbHabitant(-1);
        } catch (NbHabitant_ppq0_Exception e) {
            e.printStackTrace();
        }
        try {
            ville.setNbHabitant(5000);
        } catch (NbHabitant_ppq0_Exception e) {
            e.printStackTrace();
        }
        System.out.println(ville);
    }
}
