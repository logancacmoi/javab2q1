package exceptions_exo.ville.exceptions;

public class NbHabitant_ppq0_Exception extends Exception {
    public NbHabitant_ppq0_Exception(long n){
        super("Le nombre d'habitant doit être positif : "+n+" <= 0");

    }
}
