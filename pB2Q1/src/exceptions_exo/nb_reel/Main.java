package exceptions_exo.nb_reel;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean loop=true;
        Scanner scanner= new Scanner(System.in);
        long n=0;

        do {
            try{
                System.out.print("Entrez un nombre réel : ");
                scanner.nextLong();
                loop=false;
            }catch (InputMismatchException e){
                scanner.nextLine();
            }
        }while (loop);

    }
}
