package exceptions_exo.etudiant;

import exceptions_exo.etudiant.exceptions.*;

import java.util.ArrayList;
import java.util.List;

public class Etudiant {
    private String nom;
    private String prenom;
    private int annee;
    private List<Cours> listeCours;
    private static final int NBMAX_COURS=10;

    public Etudiant(String nom, String prenom, int annee){
        listeCours= new ArrayList<>();
        try {
            setAnnee(annee);
        } catch (CoursPresentException e) {
            e.printStackTrace();
        }
        setPrenom(prenom);
        setNom(nom);

    }

    public void ajouterCours(Cours cours) throws CoursMauvaiseAnneeException, DoublonCoursException, TropDeCoursException {
        if (cours.getAnnee()!=annee) throw new CoursMauvaiseAnneeException(this);
        if (listeCours.contains(cours)) throw new DoublonCoursException();
        if (listeCours.size()>=NBMAX_COURS) throw new TropDeCoursException(this, NBMAX_COURS);

        listeCours.add(cours);

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) throws CoursPresentException {
        if (listeCours.size()>0) throw new CoursPresentException();
        this.annee = annee>0?annee:1;
    }

    public String getCours(int ind) throws IndiceNegatifException, IndicePlusGrandQueMaxException, IndiceTropGrandException {
        if (ind<0) throw new IndiceNegatifException(ind);
        if (ind>NBMAX_COURS) throw new IndicePlusGrandQueMaxException(ind, NBMAX_COURS);
        if (listeCours.size()<ind) throw new IndiceTropGrandException(listeCours.size());

        return listeCours.get(ind).getLibelle();
    }

    public void supprimerCours(){
        listeCours.clear();
    }

    @Override
    public String toString() {
        return "Nom: "+nom+"\nPrénom: "+prenom+"\nAnnée: "+annee+"\n"+String.join("\n", listeCours.stream().map(cours -> String.format("Libellé: %-20s Année: %-2d Nombre d'heures: %2dh", cours.getLibelle(), cours.getAnnee(), cours.getNbHeures())).toArray(String[]::new));
    }
}
