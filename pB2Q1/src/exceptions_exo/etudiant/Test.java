package exceptions_exo.etudiant;

import exceptions_exo.etudiant.exceptions.CoursMauvaiseAnneeException;
import exceptions_exo.etudiant.exceptions.DoublonCoursException;
import exceptions_exo.etudiant.exceptions.TropDeCoursException;

public class Test {
    public static void main(String[] args) {
        Cours java= new Cours("Java", 2, 8);
        Cours gymnastique_artistique=new Cours("gymnastique artistique",1, 35);
        Cours dessin=new Cours("dessin",1, 35);
        Cours français=new Cours("français",1, 35);
        Cours anglais=new Cours("anglais",1, 35);
        Cours comptabilite=new Cours("comptabilite",1, 35);
        Cours ti=new Cours("ti",1, 35);
        Cours ta=new Cours("ta",1, 35);
        Cours to=new Cours("to",1, 35);
        Cours tu=new Cours("tu",1, 35);
        Cours plouf=new Cours("plouf",1, 35);
        Cours bouteille=new Cours("bouteille",1, 35);
        Etudiant etudiant1= new Etudiant("Etudiant", "1", 2);
        Etudiant etudiant2= new Etudiant("Etudiant", "2", 2);
        Etudiant etudiant3= new Etudiant("Etudiant", "3", 1);
        Etudiant etudiant4= new Etudiant("Etudiant", "4", 1);
        Etudiant etudiant5= new Etudiant("Etudiant", "5", 1);

        System.out.println("DoublonException: ");

        try {
            etudiant1.ajouterCours(java);
            etudiant1.ajouterCours(java);
        } catch (CoursMauvaiseAnneeException | DoublonCoursException | TropDeCoursException e) {
            e.printStackTrace();
        }

        System.out.println("CoursMauvaiseAnneeException: ");
        try {
            etudiant1.ajouterCours(gymnastique_artistique);
        } catch (CoursMauvaiseAnneeException | DoublonCoursException | TropDeCoursException e) {
            e.printStackTrace();
        }

        System.out.println("TropDeCoursException: ");
        try {
            etudiant3.ajouterCours(comptabilite);
            etudiant3.ajouterCours(dessin);
            etudiant3.ajouterCours(ti);
            etudiant3.ajouterCours(ta);
            etudiant3.ajouterCours(to);
            etudiant3.ajouterCours(tu);
            etudiant3.ajouterCours(plouf);
            etudiant3.ajouterCours(gymnastique_artistique);
            etudiant3.ajouterCours(anglais);
            etudiant3.ajouterCours(bouteille);
            etudiant3.ajouterCours(français);
        } catch (CoursMauvaiseAnneeException | DoublonCoursException | TropDeCoursException e) {
            e.printStackTrace();
        }

        etudiant3.supprimerCours();
        System.out.println(etudiant1+"\n");
        System.out.println(etudiant3);


    }
}
