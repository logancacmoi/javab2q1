package exceptions_exo.etudiant;

public class Cours {
    private String libelle;
    private int annee;
    private int nbHeures;


    public Cours(String libelle, int annee, int nbHeures) {
        setAnnee(annee);
        setNbHeures(nbHeures);
        setLibelle(libelle);
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        if (libelle==null) libelle="";
        this.libelle = libelle;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public int getNbHeures() {
        return nbHeures;
    }

    public void setNbHeures(int nbHeures) {
        this.nbHeures = nbHeures;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cours cours = (Cours) o;
        return annee == cours.annee &&
                libelle.equals(cours.libelle);
    }

    @Override
    public String toString() {
        return "Libellé: "+libelle+"\nAnnée: "+annee+"\nNombres d'heures: "+nbHeures;
    }
}
