package exceptions_exo.etudiant.exceptions;

public class IndicePlusGrandQueMaxException extends Exception{
    public IndicePlusGrandQueMaxException(int ind,int nbMax){
        super("L'indice : "+ind+" est plus grand que le nombre maximum de cours :"+nbMax);
    }

}
