package exceptions_exo.etudiant.exceptions;

public class CoursPresentException extends Exception {
    public CoursPresentException(){
        super("L'étudiant est encore inscrit à des cours");
    }
}
