package exceptions_exo.etudiant.exceptions;

import exceptions_exo.etudiant.Etudiant;

public class CoursMauvaiseAnneeException extends Exception {
    public CoursMauvaiseAnneeException(Etudiant etudiant) {
        super("Ce cours n'est pas accessible aux éléves cette année : " + etudiant.getAnnee());
    }
}
