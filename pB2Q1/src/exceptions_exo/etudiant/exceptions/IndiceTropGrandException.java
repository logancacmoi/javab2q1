package exceptions_exo.etudiant.exceptions;


public class IndiceTropGrandException extends Exception {
    public IndiceTropGrandException(int nbCours) {
        super("L'indice donné dépasse le nombre cours auxquels est inscrit l'étudiant : " + nbCours);

    }
}
