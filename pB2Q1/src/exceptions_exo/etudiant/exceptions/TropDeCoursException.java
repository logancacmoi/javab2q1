package exceptions_exo.etudiant.exceptions;

import exceptions_exo.etudiant.Etudiant;

public class TropDeCoursException extends Exception {
    public TropDeCoursException(Etudiant etudiant, int nbMax){
        super("L'étudiant a déjà atteind le nombre maximum de cours : "+ nbMax);
    }
}
