package exceptions_exo.etudiant.exceptions;

public class DoublonCoursException extends Exception {
    public DoublonCoursException(){
        super("L'étudiant est déjà inscrit à ce cours");
    }
}
