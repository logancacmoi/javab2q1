package exceptions_exo.etudiant.exceptions;

public class IndiceNegatifException extends Exception {
    public IndiceNegatifException(int ind){
        super("L'indice ne peut pas être négatif : "+ind);
    }
}
